//
//  JKViewController.m
//  ImageTransition
//
//  Created by Joris Kluivers on 1/12/13.
//  Copyright (c) 2013 Joris Kluivers. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "JKViewController.h"
#import "JKImageViewController.h"
#import "JKImageTransitionSegue.h"

@interface JKViewController ()

@end

@implementation JKViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.imageButton.layer.shadowColor = [UIColor blackColor].CGColor;
	self.imageButton.layer.shadowOffset = CGSizeMake(0, 1);
	self.imageButton.layer.shadowOpacity = 0.6f;
	self.imageButton.layer.shadowRadius = 1.0f;
}

- (void) viewDidAppear:(BOOL)animated
{
	self.imageButton.hidden = NO;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if (![segue isKindOfClass:[JKImageTransitionSegue class]]) {
		return;
	}
	
	JKImageViewController *imageController = (JKImageViewController *)segue.destinationViewController;
	
	// provide destination with full image
	imageController.image = [UIImage imageNamed:@"BeachFull.jpg"];
	
	// configure segue
	UIButton *imageButton = (UIButton *)sender;
	JKImageTransitionSegue *imageSegue = (JKImageTransitionSegue *)segue;
	
	imageSegue.sourceRect = imageButton.frame;
	imageSegue.transitionImage = imageButton.imageView.image;
	
	imageButton.hidden = YES;
}

- (IBAction) unwindFromSegue:(UIStoryboardSegue *)segue
{
	
}

- (UIStoryboardSegue *)segueForUnwindingToViewController:(UIViewController *)toViewController fromViewController:(UIViewController *)fromViewController identifier:(NSString *)identifier
{
	JKImageTransitionSegue *imageTransition = [[JKImageTransitionSegue alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
	imageTransition.unwinding = YES;
	imageTransition.transitionImage = self.imageButton.imageView.image;
	
	CGRect sourceRect = ((JKImageViewController *)fromViewController).imageView.frame;
	sourceRect.origin.y -= ((JKImageViewController *)fromViewController).scrollView.contentOffset.y;
	sourceRect.origin.x -= ((JKImageViewController *)fromViewController).scrollView.contentOffset.x;
	imageTransition.sourceRect = sourceRect;
	
	imageTransition.destinationRect = self.imageButton.frame;
	
	((JKImageViewController *)fromViewController).imageView.hidden = YES;
	
	return imageTransition;
}

@end
